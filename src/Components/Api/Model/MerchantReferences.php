<?php

namespace BestitKlarnaOrderManagement\Components\Api\Model;

/**
 * Representation of a Klarna merchant references as an object.
 *
 * @package BestitKlarnaOrderManagement\Components\Api\Model
 *
 * @author Ahmad El-Bardan <ahmad.el-bardan@bestit-online.de>
 */
class MerchantReferences
{
    /** @var string */
    public $merchantReference1;

    /** @var string */
    public $merchantReference2;
}
