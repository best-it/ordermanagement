<?php

namespace BestitKlarnaOrderManagement\Components\Exception;

use RuntimeException;

/**
 * This exception will be thrown if the given mode is not supported.
 *
 * @package BestitKlarnaOrderManagement\Components\Exception
 *
 * @author Senan Sharhan <senan.sharhan@bestit-online.de>
 */
class NoSupportedModeException extends RuntimeException
{
}
